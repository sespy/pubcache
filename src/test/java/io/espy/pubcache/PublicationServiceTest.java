package io.espy.pubcache;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import io.espy.pubcache.generated.pubmedfile2017.PubmedArticleSet;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = UnitTestAppConfig.class)
public class PublicationServiceTest {

	//	String miniSampleFileSpec = "src/test/resources/medline-mini-sample.xml";
	String bigSampleFileSpec = "src/test/resources/medline17n0001.xml.gz";
	//	String bigSampleFileSpec = "src/test/resources/medline16n0811.xml";
	//	String gzAltBigSampleFileSpec = "src/test/resources/medline16n0813.xml.gz";
	//	String altBigSampleFileSpec = "src/test/resources/medline16n0813.xml";

	//medline17n0001.xml.gz
	//medline17n1074.xml.gz
	//medline17n1075.xml.gz

	@Autowired
	private PublicationService publicationService;

	@Autowired
	private PubMedPublicationRepository pubMedPublicationRepository;

	@Autowired
	private FileService fileService;

	private void copyATestFileToIngest(String spec) {
		File sourceFile = new File(spec);
		File targetFile = new File(
				fileService.getPubMedRoot() + File.separator + fileService.getIngestSubDirectory() + File.separator + sourceFile.getName());
		try {
			Files.copy(sourceFile.toPath(), targetFile.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public final void trivia() {
		Assert.assertNotNull(publicationService);
		Assert.assertNotNull(pubMedPublicationRepository);
		Assert.assertNotNull(fileService);
		Assert.assertTrue(true);
	}

	@Test
	public final void testIngest() {
		pubMedPublicationRepository.deleteAll();
		Assert.assertTrue(pubMedPublicationRepository.count() == 0);
		copyATestFileToIngest(bigSampleFileSpec);
		publicationService.ingest(1);
//		Assert.assertTrue(pubMedPublicationRepository.count() > 10);
		Assert.assertTrue(true);
	}

	@Test
	public final void testUnmarshal() {
		//		String fileSpec = bigSampleFileSpec;
		String fileSpec = bigSampleFileSpec;
		//		String fileSpec = gzAltBigSampleFileSpec;
		//		String fileSpec = altBigSampleFileSpec;
		//
		pubMedPublicationRepository.deleteAll();
		//
		//		String fileSpec = fileService.getOneFileSpecToIngest();
		//		Assert.assertNotNull(fileSpec);
		if (fileSpec != null) {
			PubmedArticleSet pubmedArticleSet = publicationService.makePubmedArticleSetFromXmlFile(fileSpec);
			List<Object> objectList = pubmedArticleSet.getPubmedArticleOrPubmedBookArticle();
			//			List<PubmedArticle> hi = objectList.addAll((PubmedArticle)objectList);
			Assert.assertNotNull(pubmedArticleSet);

			//			List<PubMedPublication> pubMedPublicationList = publicationService.makePubMedPublicationListFromMedlineCitationSet(mcSet);
			//
			//			fileService.setFileSpecAsIngested(fileSpec);
			//			Assert.assertNotNull(pubMedPublicationList);
			//			System.out.println("pubMedPublicationList.size() = " + pubMedPublicationList.size());
			//
			//			for (PubMedPublication p : pubMedPublicationList) {
			//				publicationService.save(p);
			//				List<PubMedPub?otNull(foundPub);
			//			System.out.println("pubMed found = " + foundPub);
			//			}
			//		Assert.assertEquals(5, pmPublicationRepository.findAll().size());
			//						Assert.assertNotNull(pubMedPublicationList);

		}
		Assert.assertTrue(true);
	}
}
