package io.espy.pubcache;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;

public class PubMedJournal {

	@Id
	public String id;
	private String title;
	// issn linking field, which provides id regardless of format
	private String lissn;
	private String issn;
	private String essn;
	private String medlineTA;
	private Date birthday = new Date();

	public Date getBirthday() {
		return birthday;
	}

	public String getEssn() {
		return essn;
	}

	public String getId() {
		return id;
	}

	public String getIssn() {
		return issn;
	}

	public String getLissn() {
		return lissn;
	}

	public String getMedlineTA() {
		return medlineTA;
	}

	public String getTitle() {
		return title;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public void setEssn(String essn) {
		this.essn = essn;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setIssn(String issn) {
		this.issn = issn;
	}

	public void setLissn(String lissn) {
		this.lissn = lissn;
	}

	public void setMedlineTA(String medlineTA) {
		this.medlineTA = medlineTA;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

}
