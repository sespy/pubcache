package io.espy.pubcache;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@ComponentScan
@EnableSwagger2
public class AppConfig {

	private String apiDocTitle = "PubCache API";
	private String apiDocDescription = "A simple API providing metrics about publications, sourced from the NIH PubMed database.";
	private String apiContactName = "Stephen Espy";
	private String apiContactEmail = "espy@espy.net";
	private String apiVersionString = "1";
	private String emptyString = "";

	@Bean
	public Docket api() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any()).paths(paths()).build();
		docket.apiInfo(apiInfo());
		return docket;
	}

	private Predicate<String> paths() {
		Predicate<String> predicate = null;
		predicate = or(regex("/publications.*"), regex("/files.*"));
		predicate = or(predicate, regex("/meta.*"));
		return predicate;
	}

	private ApiInfo apiInfo() {
		ApiInfo apiInfo = new ApiInfo(apiDocTitle, apiDocDescription, apiVersionString, emptyString,
				new Contact(apiContactName, emptyString, apiContactEmail), emptyString, emptyString, new ArrayList<VendorExtension>());
		return apiInfo;
	}
}
