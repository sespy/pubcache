
# PubCache

### What is PubCache?

The NIH/NCBI (at the NLM) provides daily updates of publication citations related to medicine and health-focused sciences.

PubCache is a set of tools to help organizations create, and more importantly, maintain a local cache of the information provided by PubMed/Medline.


### Getting the data:  

Pubmed API docs are at [https://www.nlm.nih.gov/databases/download/pubmed_medline.html](https://www.nlm.nih.gov/databases/download/pubmed_medline.html).

`ftp://ftp.ncbi.nlm.nih.gov/pubmed/updatefiles/`

`ftp://ftp.ncbi.nlm.nih.gov/pubmed/baseline/`


### Key High-Level Tasks


REST-ful web service

	* do: download a file
	* do: check for new data
	* do: 
	* get: info for a pmid
	* get: info for an affliation?




Services
	FileService


Some of these operations are not natural targets for a web service, but it may be handy to 
execute those operations with a web request. So why not organize them this way? 
There may be a good reason not to. Tell me about that and I will change the
approach perhaps.




curl localhost:11211/api/pubcache/1/listFiles




### Notes





periodically get the latest pubmed updates and import them into a local datastore



expose a REST-ful web service that provides bibilographic data for individual publications.



### prep
untar mongo and create a 

### Notes



xml objects for unmarshal step are created with this jaxb call...
xjc -dtd -p io.espy.pubcache.generatedxml -d ../src/main/java/ nlmmedlinecitationset_160101.dtd
xjc -dtd -p io.espy.pubcache.generatedxml2017 -d ../src/main/java/ pubmed_170101.dtd


xjc -dtd -p io.espy.pubcache.generated.esummary2017 -d ../src/main/java/ esummary-v1.dtd


--


workdirectory that contains subfolders, ecah with a function. must be on the same partition/filystsme, 
and move must approimate instant effect, since to effort to handle concurrency
will be made, and if this a network mount (noit recommended) timing is less likely to cause failure.
		0
		1 donwload to here, when done, move it forward
		2 
		n move here to make avoilable for import. a file selected here is moved ahead one to import
		n moved here when selected in order to begin import.
		n move here when done importing
		n move here to archive, then replace with 1 byte, this file list is scanned to assure "we have everything"


(*) a process that downloads a file


curl http://localhost:8080/publication/26220071

