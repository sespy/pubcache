package io.espy.pubcache;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = UnitTestAppConfig.class)
public class FileServiceTest {

	public static final String A_BASELINE_FILE_NAME = "medline17n0861.xml.gz";
	public static final String A_UPDATE_FILE_NAME = "medline17n0899.xml.gz";

	@Autowired
	private FileService fileService;

	@Test
	public final void trivia() {
		Assert.assertNotNull(fileService);
		Assert.assertTrue(true);
	}

	//
	//	@Test
	//	public final void downTest() {
	//		fileService.getBaselineFile(A_BASELINE_FILE_NAME);
	//	}
	//
	
	@Test
	public final void testInitOne() {
		//		do we need a setup method that does the init?
		boolean flag = fileService.initOne();
		Assert.assertTrue(flag);
		Assert.assertTrue(true);
	}

	@Test
	public final void testGetOneFileSpecToIngest() {
		fileService.initOne();
		
		// prepare		
		String sourceFileSpec = "src/test/resources/medline17n0001.xml.gz";
		File sourceFile = new File(sourceFileSpec);
		File targetFile = null;
		
		try {
			targetFile = new File(fileService.getPubMedRoot() + "/04/" + "m001.xml.gz");
			Files.copy(sourceFile.toPath(), targetFile.toPath());
			targetFile = new File(fileService.getPubMedRoot() + "/04/" + "m002.xml.gz");
			Files.copy(sourceFile.toPath(), targetFile.toPath());
			targetFile = new File(fileService.getPubMedRoot() + "/05/" + "m101.xml.gz");
			Files.copy(sourceFile.toPath(), targetFile.toPath());
			targetFile = new File(fileService.getPubMedRoot() + "/05/" + "m102.xml.gz");
			Files.copy(sourceFile.toPath(), targetFile.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String fileSpec = "";
		fileSpec = fileService.getOneFileSpecToIngest();
		fileSpec = fileService.getOneFileSpecToIngest();
		fileSpec = fileService.getOneFileSpecToIngest();
		fileSpec = fileService.getOneFileSpecToIngest();

//		Assert.assertNotNull(fileSpec);
//
//		fileService.setFileSpecAsIngested(fileSpec);
		Assert.assertTrue(true);
	}

}
