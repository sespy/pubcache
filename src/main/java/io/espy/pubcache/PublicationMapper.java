package io.espy.pubcache;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.espy.pubcache.generated.pubmedfile2017.AffiliationInfo;
import io.espy.pubcache.generated.pubmedfile2017.Article;
import io.espy.pubcache.generated.pubmedfile2017.ArticleId;
import io.espy.pubcache.generated.pubmedfile2017.Author;
import io.espy.pubcache.generated.pubmedfile2017.AuthorList;
import io.espy.pubcache.generated.pubmedfile2017.CollectiveName;
import io.espy.pubcache.generated.pubmedfile2017.ForeName;
import io.espy.pubcache.generated.pubmedfile2017.ISSN;
import io.espy.pubcache.generated.pubmedfile2017.Initials;
import io.espy.pubcache.generated.pubmedfile2017.Journal;
import io.espy.pubcache.generated.pubmedfile2017.LastName;
import io.espy.pubcache.generated.pubmedfile2017.MedlineCitation;
import io.espy.pubcache.generated.pubmedfile2017.PubDate;
import io.espy.pubcache.generated.pubmedfile2017.PubMedPubDate;
import io.espy.pubcache.generated.pubmedfile2017.PubmedArticle;
import io.espy.pubcache.generated.pubmedfile2017.PubmedBookArticle;
import io.espy.pubcache.generated.pubmedfile2017.PubmedData;
import io.espy.pubcache.generated.pubmedfile2017.Suffix;

public class PublicationMapper {

	private static final Logger log = LoggerFactory.getLogger(PublicationMapper.class);

	public static PubMedPublication map(PubmedArticle pubmedArticle) {
		if (pubmedArticle == null) {
			return null;
		}
		MedlineCitation mCite = pubmedArticle.getMedlineCitation();
		if (mCite == null) {
			return null;
		}
		PubmedData pubmedData = pubmedArticle.getPubmedData();
		if (pubmedData == null) {
			return null;
		}

		PubMedPublication p = new PubMedPublication();
		p.setPmid(mCite.getPMID().getvalue());

		Article article = mCite.getArticle();
		if (article != null) {
			AuthorList authorListObj = article.getAuthorList();
			if (authorListObj != null) {
				List<Author> authorList = authorListObj.getAuthor();
				if (authorList != null) {
					List<PubMedAuthor> mappedAuthorList = map(authorList);
					p.setAuthorList(mappedAuthorList);
				}

				p.setArticleTitle(article.getArticleTitle().getvalue());

				Journal journal = article.getJournal();
				if (journal != null) {
					PubMedJournal pubMedJournal = PublicationMapper.map(journal);
					p.setJournal(pubMedJournal);
				}
			}
		}

		List<PubMedPubDate> pubMedPubDateList = pubmedData.getHistory().getPubMedPubDate();

		List<ArticleId> articleIdList = pubmedData.getArticleIdList().getArticleId();
		for (ArticleId articleId : articleIdList) {
			String type = articleId.getIdType();
			String value = articleId.getvalue();
			if ("doi".equals(type)) {
				p.setDoi(value);
			} else
				if ("pubmed".equals(type)) {
					// punt		
				} else
					if ("pii".equals(type)) {
						// punt
					} else
						if ("mid".equals(type)) {
							p.setMid(value);
						} else
							if ("pmc".equals(type)) {
								p.setPmcid(value);
							} else {
								log.debug("unrecognized id type = " + type);
							}
		}

		if (pubMedPubDateList != null) {
			for (PubMedPubDate d : pubMedPubDateList) {
				if ("pubmed".equals(d.getPubStatus())) {
					p.setDateCreatedYYYY(d.getYear().getvalue());
					p.setDateCreatedMM(d.getMonth().getvalue());
					p.setDateCreatedDD(d.getDay().getvalue());

				} else
					if ("received".equals(d.getPubStatus())) {

					} else
						if ("revised".equals(d.getPubStatus())) {

						} else
							if ("accepted".equals(d.getPubStatus())) {

							} else
								if ("medline".equals(d.getPubStatus())) {

								} else
									if ("entrez".equals(d.getPubStatus())) {

									} else {
										log.debug("unrecognized date type = " + d.getPubStatus());
									}
			}
		}

		//p.setMid("");
		//p.setPmcid("");
		//p.setDateCreatedDD("");
		//p.setDateCreatedMM("");
		//p.setDateCreatedYYYY("");
		//log.debug("mapped pmid = " + p.getPmid());
		//log.debug("pubMedPubDateList = " + pubMedPubDateList);

		return p;
	}

	public static PubMedPublication map(PubmedBookArticle pubmedBookArticle) {
		System.err.println("i don't know how to map pubmed book articles yet.");
		return null;
	}

	public static PubMedAuthor mapAA(Author author) {
		PubMedAuthor pma = new PubMedAuthor();
		List<AffiliationInfo> affiliationInfoList = author.getAffiliationInfo();
		String affiliationListString = "";
		if (affiliationInfoList == null) {
			log.debug("affiliationInfoList null");
		} else {
			for (AffiliationInfo ai : affiliationInfoList) {
				String s = ai.getAffiliation();
				affiliationListString = affiliationListString + s + ",";
			}
		}
		pma.setAffiliation(affiliationListString);
		//		if (author.getLastNameOrForeNameOrInitialsOrSuffixOrCollectiveName typeof (new LastName()))
		pma.setLastName(author.getLastNameOrForeNameOrInitialsOrSuffixOrCollectiveName().toString());
		author.getAffiliationInfo();
		return pma;
	}

	public static List<PubMedAuthor> map(List<Author> authorList) {
		List<PubMedAuthor> mappedAuthorList = new ArrayList<PubMedAuthor>();
		for (Author a : authorList) {
			PubMedAuthor mappedAuthor = map(a);
			mappedAuthorList.add(mappedAuthor);
		}
		return mappedAuthorList;
	}

	public static PubMedJournal map(Journal j) {
		PubMedJournal pubMedJournal = new PubMedJournal();
		pubMedJournal.setTitle(j.getTitle());
		PubDate d = j.getJournalIssue().getPubDate();
		//TODO use the PubDate
		ISSN issn = j.getISSN();
		if (issn != null) {
			if ("Electronic".equals(issn.getIssnType())) {
				pubMedJournal.setEssn(j.getISSN().getvalue());
			}
		}
		return pubMedJournal;
	}

	//	public void dasfjlk(AuthorList authorList) {
	//	AuthorList authorList = article.getAuthorList();
	//	if (authorList != null) {
	//		for (Author author : authorList.getAuthor()) {

	public static PubMedAuthor map(Author author) {
		PubMedAuthor pa = new PubMedAuthor();
		String valid = author.getValidYN();
		if (valid != null && valid.equals("Y")) {
			List<Object> names = author.getLastNameOrForeNameOrInitialsOrSuffixOrCollectiveName();

			String firstName = null;
			@SuppressWarnings("unused")
			String initials = null;
			String lastName = null;
			String suffix = null;
			String collectiveName = null;

			for (Object name : names) {
				if (name instanceof LastName) {
					LastName ln = (LastName) name;
					lastName = ln.getvalue();
				} else
					if (name instanceof ForeName) {
						ForeName fn = (ForeName) name;
						firstName = fn.getvalue();
					} else
						if (name instanceof Initials) {
							Initials it = (Initials) name;
							initials = it.getvalue();
						} else
							if (name instanceof Suffix) {
								Suffix sf = (Suffix) name;
								suffix = sf.getvalue();
							} else
								if (name instanceof CollectiveName) {
									CollectiveName cn = (CollectiveName) name;
									collectiveName = cn.getvalue();
								}

			}
			pa.setLastName(lastName);
			pa.setForeName(firstName);
			pa.setSuffix(suffix);
			//				if (collectiveName != null) {
			//					document.addAuthor(collectiveName);
			//				}
			//				if (firstName != null && lastName != null && suffix != null) {
			//					String authorName = firstName + " " + lastName + " " + suffix;
			//					document.addAuthor(authorName);
			//				}
			//				if (firstName != null && lastName != null) {
			//					String authorName = firstName + " " + lastName;
			//					document.addAuthor(authorName);
			//				}

		}
		return pa;
	}

}
