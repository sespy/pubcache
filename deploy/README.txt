# README.txt
# audience: Operations team members who want to know what this
# service provides from this directory.

service: api-pubcache

This service provides a web services API that supplies 
information from local database that reflects the content
of the PubMed database, sourced from the NIH.
