package io.espy.pubcache;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;

public class PubMedPublication {

	public String id;
	@Id
	public String pmid;
	private String articleTitle;
	private String doi;
	private String mid;
	private String pmcid;
	private String dateCreatedYYYY;
	private String dateCreatedMM;
	private String dateCreatedDD;
	private List<PubMedAuthor> authorList;
	private PubMedJournal journal;
	private Date birthday = new Date();

	public String getArticleTitle() {
		return articleTitle;
	}

	public List<PubMedAuthor> getAuthorList() {
		return authorList;
	}

	public Date getBirthday() {
		return birthday;
	}

	public String getDateCreatedDD() {
		return dateCreatedDD;
	}

	public String getDateCreatedMM() {
		return dateCreatedMM;
	}

	public String getDateCreatedYYYY() {
		return dateCreatedYYYY;
	}

	public String getDoi() {
		return doi;
	}

	public String getId() {
		return id;
	}

	public PubMedJournal getJournal() {
		return journal;
	}

	public String getMid() {
		return mid;
	}

	public String getPmcid() {
		return pmcid;
	}

	public String getPmid() {
		return pmid;
	}

	public void setArticleTitle(String articleTitle) {
		this.articleTitle = articleTitle;
	}

	public void setAuthorList(List<PubMedAuthor> authorList) {
		this.authorList = authorList;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public void setDateCreatedDD(String dateCreatedDD) {
		this.dateCreatedDD = dateCreatedDD;
	}

	public void setDateCreatedMM(String dateCreatedMM) {
		this.dateCreatedMM = dateCreatedMM;
	}

	public void setDateCreatedYYYY(String dateCreatedYYYY) {
		this.dateCreatedYYYY = dateCreatedYYYY;
	}

	public void setDoi(String doi) {
		this.doi = doi;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setJournal(PubMedJournal journal) {
		this.journal = journal;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public void setPmcid(String pmcid) {
		this.pmcid = pmcid;
	}

	public void setPmid(String pmid) {
		this.pmid = pmid;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

}
