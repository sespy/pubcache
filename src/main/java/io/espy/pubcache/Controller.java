package io.espy.pubcache;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	private static final Logger log = LoggerFactory.getLogger(Controller.class);

	@Autowired
	PubMedPublicationRepository pubMedPublicationRepository;

	@Autowired
	FileService fileService;

	@Autowired
	PublicationService publicationService;

	@RequestMapping(value = "/publications/{pmid}", method = RequestMethod.GET)
	public List<PubMedPublication> findPub(@PathVariable("pmid") String pmidString) {
		List<PubMedPublication> pmpList = pubMedPublicationRepository.findById(pmidString);
		log.debug("number of publications found = " + pmpList.size());
		return pmpList;
	}

	@PostMapping(value = "/files/ingest")
	public String ingest(@RequestParam(value = "n", required = false) Integer n) {
		if (n == null) {
			n = new Integer(0);
		}
		publicationService.ingest(n);
		return "ok";
	}

	@GetMapping(value = "/files/listMissingUpdates")
	public Collection<String> fileListMissingUpdates() {
		Collection<String> f = fileService.listMissingUpdateFiles();
		return f;
	}

	@GetMapping(value = "/files/listMissingBaselineFiles")
	public Collection<String> filesListMissingBaselineFiles() {
		Collection<String> f = fileService.listMissingBaselineFiles();
		return f;
	}

	@PostMapping(value = "/files/downloadMissingUpdates")
	public String filesDownloadMissingUpates(@RequestParam(value = "n", required = false) Integer n) {
		if (n == null) {
			n = new Integer(0);
		}
		fileService.downloadMissingUpdateFiles(n);
		return "ok";
	}

	@PostMapping(value = "/files/downloadMissingBaseline")
	public String filesDownloadMissingBaseline(@RequestParam(value = "n", required = false) Integer n) {
		if (n == null) {
			n = new Integer(0);
		}
		fileService.downloadMissingBaselineFiles(n);
		return "ok";
	}

	@GetMapping(value = "/meta/stats")
	public Map<String, String> basicStats() {
		return publicationService.basicStats();
	}

	@GetMapping(value = "/meta/isPaused")
	public boolean isPaused() {
		boolean b = PublicationService.isPaused();
		return b;
	}

	@PostMapping(value = "/meta/pause")
	public String pause() {
		PublicationService.setPaused(true);
		return "ok";
	}

	@PostMapping(value = "/meta/unpause")
	public String unpause() {
		PublicationService.setPaused(false);
		return "ok";
	}

}
