package io.espy.pubcache;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;

public class PubMedAuthor {

	@Id
	public String id;
	// forename is the part of the name that is not the last name
	private String foreName;
	private String lastName;
	// up to two
	private String initials;
	// like jr, sr
	private String suffix;
	private String collectiveName;
	private String orcid;
	private String affiliation;
	private Date birthday = new Date();

	public String getAffiliation() {
		return affiliation;
	}

	public Date getBirthday() {
		return birthday;
	}

	public String getCollectiveName() {
		return collectiveName;
	}

	public String getForeName() {
		return foreName;
	}

	public String getId() {
		return id;
	}

	public String getInitials() {
		return initials;
	}

	public String getLastName() {
		return lastName;
	}

	public String getOrcid() {
		return orcid;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public void setCollectiveName(String collectiveName) {
		this.collectiveName = collectiveName;
	}

	public void setForeName(String foreName) {
		this.foreName = foreName;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setOrcid(String orcid) {
		this.orcid = orcid;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

}
