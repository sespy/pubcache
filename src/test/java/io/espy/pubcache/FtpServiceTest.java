package io.espy.pubcache;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = UnitTestAppConfig.class)
public class FtpServiceTest {

	@Autowired
	private FtpService ftpService;

	@Test
	public final void trivia() {
		Assert.assertNotNull(ftpService);
		Assert.assertNotNull("");
		Assert.assertTrue(true);
	}

	@Test
	public final void testGetBaselineFileList() {
		List<String> fileNameStringList = ftpService.getBaselineFileList();
		Assert.assertNotNull(fileNameStringList);
		//		 for the rest of the year, this should pass just fine.
		Assert.assertTrue(fileNameStringList.size() > 100);
	}

}
