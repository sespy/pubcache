package io.espy.pubcache;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class FileService {

	private static final Logger log = LoggerFactory.getLogger(FileService.class);

	private static final int MAX = 1000;
	@Autowired
	FtpService ftpService;

	// The pubMedRoot is assumed to be the root of all of the other
	// directories. The otherDirectory is at <pubMedRoot>/otherDirectory
	@Value("${io.espy.pubcache.pubMedRoot}")
	private String pubMedRoot;

	@Value("${io.espy.pubcache.ingestSubDir}")
	private String ingestSubDirectory;

	@Value("${io.espy.pubcache.workSubDir}")
	private String workSubDirectory;

	@Value("${io.espy.pubcache.nihFileSuffix}")
	private String nihFileSuffix;

	File pubMedRootFile;

	File dowloadBaselineIncomingDirectory02File;
	File dowloadUpdateIncomingDirectory03File;
	File dowloadedBaselineDirectory04File;
	File dowloadedUpdateDirectory05File;

	// files are moved here when they are selected for ingest.
	File stagedForIngestDirectory10File;
	// when ingest from 10 is complete, the file is moved here
	File postIngestDirectory11File;

	File archivedBaselineDirectory21File;
	File archivedUpdateDirectory22File;

	private static boolean isInitializationSuccessful = false;

	public boolean downloadMissingBaselineFiles(int n) {
		boolean isChangeAffected = false;
		Collection<String> missingFileNameCollection = listMissingBaselineFiles();
		for (String s : missingFileNameCollection) {
			if (n > 0) {
				n--;
				getBaselineFile(s);
				File from = new File(dowloadBaselineIncomingDirectory02File.getPath() + File.separator + s);
				File to = new File(dowloadedBaselineDirectory04File.getPath() + File.separator + s);
				move(from, to);
				isChangeAffected = true;
			}
		}
		return isChangeAffected;
	}

	public boolean downloadMissingUpdateFiles(Integer n) {
		boolean isChangeAffected = false;

		initOne();
		if (n == null || n < 1) {
			n = MAX;
		}
		Collection<String> missingFileNameCollection = listMissingUpdateFiles();
		for (String s : missingFileNameCollection) {
			if (n > 0) {
				n--;
				getUpdateFile(s);
				File f = new File(dowloadUpdateIncomingDirectory03File.getPath() + File.separator + s);
				File to = new File(dowloadedUpdateDirectory05File.getPath() + File.separator + s);
				move(f, to);
				isChangeAffected = true;
			}
		}
		return isChangeAffected;
	}

	private void move(File sourceFile, File targetFile) {
		try {
			log.debug("moving file from " + sourceFile.toPath() + " to " + targetFile.toPath());
			Files.move(sourceFile.toPath(), targetFile.toPath(), StandardCopyOption.ATOMIC_MOVE);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean getBaselineFile(String sourceFileSpec) {
		initOne();
		boolean isOk = ftpService.getBaselineFile(sourceFileSpec, dowloadBaselineIncomingDirectory02File.getPath());
		return isOk;
	}

	private boolean getUpdateFile(String sourceFileSpec) {
		boolean isOk = ftpService.getUpdateFile(sourceFileSpec, dowloadUpdateIncomingDirectory03File.getPath());
		return isOk;
	}

	private Collection<String> getKnownFileNameCollection() {
		initOne();
		List<String> runningCollectionOfFileNames = new ArrayList<String>();
		runningCollectionOfFileNames.addAll(getFileNameListForDirectoryFile(dowloadBaselineIncomingDirectory02File));
		runningCollectionOfFileNames.addAll(getFileNameListForDirectoryFile(dowloadUpdateIncomingDirectory03File));
		runningCollectionOfFileNames.addAll(getFileNameListForDirectoryFile(dowloadedBaselineDirectory04File));
		runningCollectionOfFileNames.addAll(getFileNameListForDirectoryFile(dowloadedUpdateDirectory05File));
		runningCollectionOfFileNames.addAll(getFileNameListForDirectoryFile(stagedForIngestDirectory10File));
		runningCollectionOfFileNames.addAll(getFileNameListForDirectoryFile(postIngestDirectory11File));
		runningCollectionOfFileNames.addAll(getFileNameListForDirectoryFile(archivedBaselineDirectory21File));
		runningCollectionOfFileNames.addAll(getFileNameListForDirectoryFile(archivedUpdateDirectory22File));
		Collections.reverse(runningCollectionOfFileNames);
		return runningCollectionOfFileNames;
	}

	public Collection<String> listMissingBaselineFiles() {
		List<String> fileNameList = new ArrayList<String>();
		List<String> baselineFileNameStringList = ftpService.getBaselineFileList();
		log.debug("baselineFileNameStringList size = " + baselineFileNameStringList.size());
		for (String s : baselineFileNameStringList) {
			if (s != null && s.endsWith(nihFileSuffix)) {
				fileNameList.add(s);
			}
		}
		log.debug("baseline fileNameList has size = " + fileNameList.size());
		Collection<String> completedFileNameStringList = getKnownFileNameCollection();
		log.debug("completedFileNameStringList size = " + completedFileNameStringList.size());
		fileNameList.removeAll(completedFileNameStringList);
		log.debug("baseline fileNameList after removing done has size = " + fileNameList.size());
		log.debug("returning missing baseline file name list size = " + fileNameList.size());
		return fileNameList;
	}

	public Collection<String> listMissingUpdateFiles() {
		List<String> fileNameList = new ArrayList<String>();
		List<String> updateFileNameStringList = ftpService.getUpdateFileList();
		log.debug("listMissingUpdateFiles: remote update updateFileNameStringList has size = " + updateFileNameStringList.size());
		for (String s : updateFileNameStringList) {
			if (s != null && s.endsWith(nihFileSuffix)) {
				fileNameList.add(s);
			}
		}
		log.debug("listMissingUpdateFiles: fileNameList starts with size = " + fileNameList.size());
		Collection<String> completedFileNameStringList = getKnownFileNameCollection();
		log.debug("listMissingUpdateFiles: local completedFileNameStringList size = " + completedFileNameStringList.size());
		fileNameList.removeAll(completedFileNameStringList);
		log.debug("listMissingUpdateFiles: after removing known files, fileNameList size = " + fileNameList.size());
		return fileNameList;
	}

	private File makeDirectoryIfMissing(String dirPath) {
		File directoryFile = null;
		try {
			directoryFile = new File(dirPath);
			if (!directoryFile.exists() || !directoryFile.isDirectory()) {
				log.info("directory " + dirPath + " does not exist or is not a directory; creating...");
				Files.createDirectories(directoryFile.toPath());
				log.info("directory " + dirPath + " created.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return directoryFile;
	}

	public boolean initOne() {
		log.debug("initOne() begin");
		if (isInitializationSuccessful) {
			log.trace("initialized previously, so skipping intitalization.");
			return true;
		}
		pubMedRootFile = makeDirectoryIfMissing(pubMedRoot);
		dowloadBaselineIncomingDirectory02File = makeDirectoryIfMissing(pubMedRoot + File.separator + "02");
		dowloadUpdateIncomingDirectory03File = makeDirectoryIfMissing(pubMedRoot + File.separator + "03");
		dowloadedBaselineDirectory04File = makeDirectoryIfMissing(pubMedRoot + File.separator + "04");
		dowloadedUpdateDirectory05File = makeDirectoryIfMissing(pubMedRoot + File.separator + "05");
		stagedForIngestDirectory10File = makeDirectoryIfMissing(pubMedRoot + File.separator + "10");
		postIngestDirectory11File = makeDirectoryIfMissing(pubMedRoot + File.separator + "11");
		archivedBaselineDirectory21File = makeDirectoryIfMissing(pubMedRoot + File.separator + "21");
		archivedUpdateDirectory22File = makeDirectoryIfMissing(pubMedRoot + File.separator + "22");
		if (pubMedRootFile != null) {
			isInitializationSuccessful = true;
		}
		return isInitializationSuccessful;
	}

	public Collection<String> getFileNameListForDirectoryFile(File directoryFile) {
		initOne();
		Collection<String> fileNameCollection = new ArrayList<String>();
		log.info("directoryFile path =" + directoryFile.getAbsolutePath());

		Collection<File> fileCollection = FileUtils.listFiles(directoryFile, null, false);
		log.info("size fileCollection =" + fileCollection.size());

		for (File f : fileCollection) {
			fileNameCollection.add(f.getName());
		}

		return fileNameCollection;
	}

	/***
	 * 
	 * If there is at least one file in the ingest directory, provide one 
	 * absolute path as a String for one of those files. Returns null if 
	 * we cannot initalize, or if there are no files in that directory.
	 * @return
	 */
	public String getOneFileSpecToIngest() {
		if (!initOne()) {
			log.info("Initalization failed, cannot ingest");
			return null;
		} else {
			log.info("Initalization successful, ready to ingest");
		}

		File f = getOneFileToIngest();

		if (f == null) {
			return null;
		}

		return f.getAbsolutePath();
	}

	/*
	 * Obtain a file reference for a file in the ingest directory. Provides 
	 * a java.nio.File for a file ending with one of the set extensions. When
	 * the reference is obtained, the file is moved to the work directory 0.
	 * 
	 */
	private File getOneFileToIngest() {
		String[] extensionsStringArray = new String[] { "xml", "gz" };
		Collection<File> filesBaselineCollection = FileUtils.listFiles(dowloadedBaselineDirectory04File, extensionsStringArray, false);

		if (filesBaselineCollection == null || filesBaselineCollection.isEmpty()) {
			log.debug("dowloadedBaselineDirectory04File contains no files with extensionsStringArray; dir = "
					+ dowloadedBaselineDirectory04File.toPath());
			filesBaselineCollection = FileUtils.listFiles(dowloadedUpdateDirectory05File, extensionsStringArray, false);
			if (filesBaselineCollection == null || filesBaselineCollection.isEmpty()) {
				log.debug("dowloadedUpdateDirectory05File contains no files with extensionsStringArray; dir = "
						+ dowloadedUpdateDirectory05File.toPath());
				return null;
			}
		}

		File f = filesBaselineCollection.iterator().next();
		File targetFile = new File(stagedForIngestDirectory10File.getAbsolutePath() + File.separator + f.getName());
		move(f, targetFile);

		log.debug("returning absolute path fileSpec = " + targetFile.getAbsolutePath());
		return targetFile;
	}

	public void setFileSpecAsIngested(String fileSpec) {
		File sourceFile = new File(fileSpec);
		File targetFile = new File(postIngestDirectory11File.getAbsolutePath() + File.separator + sourceFile.getName());
		move(sourceFile, targetFile);
	}

	public Map<String, String> stats() {
		Map<String, String> map = new HashMap<String, String>();
		if (!initOne()) {
			return map;
		}

		map.put("dowloadBaselineIncomingDirectory02File", "" + FileUtils.listFiles(dowloadBaselineIncomingDirectory02File, null, false).size());
		map.put("dowloadUpdateIncomingDirectory03File", "" + FileUtils.listFiles(dowloadUpdateIncomingDirectory03File, null, false).size());
		map.put("dowloadedBaselineDirectory04File", "" + FileUtils.listFiles(dowloadedBaselineDirectory04File, null, false).size());
		map.put("dowloadedUpdateDirectory05File", "" + FileUtils.listFiles(dowloadedUpdateDirectory05File, null, false).size());
		map.put("stagedForIngestDirectory10File", "" + FileUtils.listFiles(stagedForIngestDirectory10File, null, false).size());
		map.put("postIngestDirectory11File", "" + FileUtils.listFiles(postIngestDirectory11File, null, false).size());
		map.put("archivedBaselineDirectory21File", "" + FileUtils.listFiles(archivedBaselineDirectory21File, null, false).size());
		map.put("archivedUpdateDirectory22File", "" + FileUtils.listFiles(archivedUpdateDirectory22File, null, false).size());

		return map;
	}

	public String getPubMedRoot() {
		return pubMedRoot;
	}

	public void setPubMedRoot(String pubMedRoot) {
		this.pubMedRoot = pubMedRoot;
	}

	public String getIngestSubDirectory() {
		return ingestSubDirectory;
	}

	public void setIngestSubDirectory(String ingestSubDirectory) {
		this.ingestSubDirectory = ingestSubDirectory;
	}

	public String getWorkSubDirectory() {
		return workSubDirectory;
	}

	public void setWorkSubDirectory(String workSubDirectory) {
		this.workSubDirectory = workSubDirectory;
	}

}