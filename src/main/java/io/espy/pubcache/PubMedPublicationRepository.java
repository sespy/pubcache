package io.espy.pubcache;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "pmp", path = "pmp")
public interface PubMedPublicationRepository extends MongoRepository<PubMedPublication, String> {

	List<PubMedPublication> findByPmid(@Param("pubmedid") String pmidd);

	List<PubMedPublication> findById(String pmidString);

}