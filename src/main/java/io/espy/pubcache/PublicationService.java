package io.espy.pubcache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import io.espy.pubcache.generated.pubmedfile2017.PubmedArticle;
import io.espy.pubcache.generated.pubmedfile2017.PubmedArticleSet;
import io.espy.pubcache.generated.pubmedfile2017.PubmedBookArticle;

@Service
public class PublicationService {
	private static final Logger log = LoggerFactory.getLogger(PublicationService.class);

	@Autowired
	private PubMedPublicationRepository pubMedPublicationRepository;
	@Autowired
	private FileService fileService;
	private static boolean paused;

	private Date birthday = new Date();

	@Scheduled(cron = "${io.espy.pubcache.fileRefreshCron}")
	public void routineRefreshJob() {
		if (isPaused()) {
			log.debug("routineRefreshJob skipping, isPause = true");
			return;
		}
		log.debug("routineRefreshJob start-refresh");
		if (!fileService.downloadMissingBaselineFiles(1)) {
			fileService.downloadMissingUpdateFiles(1);
		}
		log.debug("routineRefreshJob end-refresh");
	}

	public static void setPaused(boolean paused) {
		PublicationService.paused = paused;
	}

	public static boolean isPaused() {
		return paused;
	}

	public Map<String, String> basicStats() {
		Map<String, String> statMap = new HashMap<String, String>();
		statMap.put("birthday", "" + birthday);
		statMap.put("pubmed-count", "" + pubMedPublicationRepository.count());
		Map<String, String> fileMap = fileService.stats();
		if (fileMap != null) {
			for (String s : fileMap.keySet()) {
				statMap.put(s, fileMap.get(s));
			}
		}

		return statMap;
	}

	@Scheduled(cron = "${io.espy.pubcache.fileIngestCron}")
	public void ingest() {
		if (isPaused()) {
			log.debug("ingest skipping, isPause = true");
			return;
		}
		ingest(0);
	}

	public void ingest(int n) {
		if (n < 1) {
			n = 1;
		}

		int i = 0;
		while (i < n) {
			String fileSpec = fileService.getOneFileSpecToIngest();

			// ingest it
			if (fileSpec != null) {
				PubmedArticleSet pubmedArticleSet = makePubmedArticleSetFromXmlFile(fileSpec);
				log.debug("unmarshalled size = " + pubmedArticleSet.getPubmedArticleOrPubmedBookArticle().size());

				List<PubMedPublication> pubMedPublicationList = makePubMedPublicationListFromArticleSet(pubmedArticleSet);
				log.debug("mapped pubMedPublicationList size = " + pubMedPublicationList.size());

				pubMedPublicationRepository.save(pubMedPublicationList);

				// set file as ingested which moves it to work/1
				fileService.setFileSpecAsIngested(fileSpec);
			}
			i++;
		}
	}

	public PubmedArticleSet makePubmedArticleSetFromXmlFile(String fileSpec) {
		if (fileSpec == null || fileSpec.isEmpty()) {
			return null;
		}
		PubmedArticleSet pubmedArticleSet = null;

		if (fileSpec.endsWith(".gz")) {
			try {
				pubmedArticleSet = unMarshalMedlineIS(fileSpec);
			} catch (JAXBException e) {
				e.printStackTrace();
			}
		} else {
			File f = new File(fileSpec);
			try {
				pubmedArticleSet = unMarshalMedlineFile(f);
			} catch (JAXBException e) {
				e.printStackTrace();
			}
		}
		log.debug("mapped pubmedArticleSet with size = " + pubmedArticleSet.getPubmedArticleOrPubmedBookArticle().size());

		return pubmedArticleSet;
	}

	public List<PubMedPublication> makePubMedPublicationListFromArticleSet(PubmedArticleSet pubmedArticleSet) {
		List<Object> articleObjectList = pubmedArticleSet.getPubmedArticleOrPubmedBookArticle();
		if (articleObjectList == null) {
			log.debug("articleObjectList was null");
		}
		if (articleObjectList.isEmpty()) {
			log.debug("articleObjectList has no elements");
		}

		//		articleObjectList = articleObjectList.subList(articleObjectList.size() - 100, articleObjectList.size() - 1);

		List<PubmedArticle> pubmedArticleList = new ArrayList<PubmedArticle>();
		List<PubmedBookArticle> pubmedBookArticleList = new ArrayList<PubmedBookArticle>();

		for (Object o : articleObjectList) {
			if (o.getClass().getClass().isInstance(PubmedArticle.class)) {
				pubmedArticleList.add((PubmedArticle) o);
			} else
				if (o.getClass().getClass().isInstance(PubmedBookArticle.class)) {
					pubmedBookArticleList.add((PubmedBookArticle) o);
				} else {
					log.debug("unrecognized type " + o.getClass().getCanonicalName());
				}
		}
		//		List<PubMedPublication> journalArticleList = new ArrayList<PubMedPublication>();
		List<PubMedPublication> journalArticleList = dealWithJournalArticles(pubmedArticleList);
		//		List<PubMedPublication> bookArticleList = dealWithBooks(pubmedBookArticleList);

		return journalArticleList;
	}

	private List<PubMedPublication> dealWithJournalArticles(List<PubmedArticle> pubmedArticleList) {
		if (pubmedArticleList == null) {
			return null;
		}
		List<PubMedPublication> publicationList = new ArrayList<PubMedPublication>();
		for (PubmedArticle p : pubmedArticleList) {

			publicationList.add(dealWithJournalArticle(p));
		}
		return publicationList;
	}

	private PubMedPublication dealWithJournalArticle(PubmedArticle pubmedArticle) {
		PubMedPublication pubMedPublication = PublicationMapper.map(pubmedArticle);
//		log.trace("pubMedPublication = " + pubMedPublication);
		return pubMedPublication;
	}

	private List<PubMedPublication> dealWithBooks(List<PubmedBookArticle> pubmedBookArticleList) {
		List<PubMedPublication> publicationList = new ArrayList<PubMedPublication>();

		for (PubmedBookArticle pubmedBookArticle : pubmedBookArticleList) {
			publicationList.add(dealWithBook(pubmedBookArticle));

		}
		return publicationList;
	}

	private PubMedPublication dealWithBook(PubmedBookArticle pubmedBookArticle) {
		log.debug("pubmedBookArticle = " + pubmedBookArticle);
		PubMedPublication p = PublicationMapper.map(pubmedBookArticle);
		return p;
	}

	private static PubmedArticleSet unMarshalMedlineFile(File medlineFile) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(PubmedArticleSet.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		System.setProperty("javax.xml.accessExternalDTD", "http, https");
		PubmedArticleSet pubmedArticleSet = (PubmedArticleSet) jaxbUnmarshaller.unmarshal(medlineFile);
		System.clearProperty("javax.xml.accessExternalDTD");
		return pubmedArticleSet;
	}

	private static PubmedArticleSet unMarshalMedlineIS(String fileSpec) throws JAXBException {

		FileInputStream fin = null;
		try {
			fin = new FileInputStream(fileSpec);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		GZIPInputStream gzis = null;
		try {
			gzis = new GZIPInputStream(fin);
		} catch (IOException e) {
			e.printStackTrace();
		}

		JAXBContext jaxbContext = JAXBContext.newInstance(PubmedArticleSet.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		System.setProperty("javax.xml.accessExternalDTD", "http, https");
		PubmedArticleSet pubmedArticleSet = (PubmedArticleSet) jaxbUnmarshaller.unmarshal(gzis);
		System.clearProperty("javax.xml.accessExternalDTD");
		return pubmedArticleSet;
	}

}