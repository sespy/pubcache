package io.espy.pubcache;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class FtpService {

	private static final Logger log = LoggerFactory.getLogger(FtpService.class);

	@Value("${pubcache.nih.pubmedXmlHost}")
	private String host;

	@Value("${pubcache.nih.pubmedXmlUpdatePath}")
	private String updatePath;

	@Value("${pubcache.nih.pubmedXmlBaselinePath}")
	private String baselinePath;

	@Value("${pubcache.nih.ftpLoginUserName}")
	private String userName;

	@Value("${pubcache.nih.ftpLoginPassword}")
	private String password;

	public List<String> getUpdateFileList() {
		FTPClient ftpClient = getNihFtpClient();
		return getFileNameListFromFtpDirectoryName(ftpClient, updatePath);
	}

	public List<String> getBaselineFileList() {
		FTPClient ftpClient = getNihFtpClient();
		return getFileNameListFromFtpDirectoryName(ftpClient, baselinePath);
	}

	public boolean getBaselineFile(String sourceFileSpec, String destiationPathSpec) {
		String destiationFileSpec = destiationPathSpec + File.separator + sourceFileSpec;
		sourceFileSpec = baselinePath + "/" + sourceFileSpec;
		return getFile(sourceFileSpec, destiationFileSpec);
	}

	public boolean getUpdateFile(String sourceFileSpec, String destiationPathSpec) {
		String destiationFileSpec = destiationPathSpec + File.separator + sourceFileSpec;
		sourceFileSpec = updatePath + "/" + sourceFileSpec;
		return getFile(sourceFileSpec, destiationFileSpec);
	}

	private boolean getFile(String sourceFileSpec, String destinationFileSpec) {
		FTPClient ftp = null;
		log.debug("sourceFileSpec=" + sourceFileSpec + "; destinationFileSpec=" + destinationFileSpec);
		Long startTime = (new Date()).getTime();
		try {
			ftp = getNihFtpClient();
			retrieveFile(ftp, sourceFileSpec, destinationFileSpec);
			Long delta = (new Date()).getTime() - startTime;
			log.debug("transferred in " + delta + " millis");
			ftp.logout();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (ftp.isConnected()) {
				try {
					ftp.disconnect();
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}
		return true;
	}

	private FTPClient getNihFtpClient() {
		FTPClient ftp = new FTPClient();
		// FTPClientConfig config = new FTPClientConfig();
		// ftp.configure(config);
		try {
			ftp.connect(host);
			ftp.setFileType(FTP.BINARY_FILE_TYPE);
			ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);
			ftp.login(userName, password);

			ftp.enterLocalPassiveMode();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ftp;
	}

	private void retrieveFile(FTPClient ftp, String sourceFileSpec, String destiationFileSpec) throws IOException {
		OutputStream output;
		output = new FileOutputStream(destiationFileSpec);
		ftp.retrieveFile(sourceFileSpec, output);
		output.close();
	}

	private List<String> getFileNameListFromFtpDirectoryName(FTPClient ftpClient, String directoryName) {
		if (directoryName == null) {
			directoryName = "" + File.separator;
		}
		FTPFile[] files = null;
		try {

			files = ftpClient.listFiles(directoryName);
			ftpClient.logout();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (ftpClient.isConnected()) {
				try {
					ftpClient.disconnect();
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		if (files == null) {
			log.debug("file list was null");
			return null;
		}

		List<String> fileNameStringList = new ArrayList<String>();
		if (files.length == 0) {
			log.debug("file list had no entries");
			return fileNameStringList;
		}

		for (FTPFile f : files) {
			fileNameStringList.add(f.getName());
		}
		return fileNameStringList;
	}

}
